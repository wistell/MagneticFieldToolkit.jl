using MagneticFieldToolkit
using StaticArrays
using CoordinateTransformations
using Test

@testset "MagneticFieldToolkit.jl" begin
  include("coil_tests.jl")
end
